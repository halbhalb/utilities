#!/usr/bin/env python
import os

new_size="1024x768"
paths = os.environ["NAUTILUS_SCRIPT_SELECTED_FILE_PATHS"].splitlines()

for path in paths:
    if os.system("identify "+path):
        continue
    parts = path.rsplit('/',1)
    new_path = parts[0]+'/'+new_size+'_'+parts[1]
    convert_command =  'convert "'+path+'" -resize '+new_size+' "'+new_path+'"'
    os.system(convert_command)
