#!/usr/bin/env python
import flickrapi
import os, os.path
import hashlib
import re
import string
import sys
import traceback
import webbrowser
from xml.etree import ElementTree

api_key = 'c36b8199bd2f58f1498c0b8db89dd2c5'
api_secret = '71d68bd454f17983'
root_directory = '.'


class Sync:
	flickr = None
	root_directory = None
	file_pattern = r".+\.(jpg|jpeg|png)$"

	def __init__(self, flickr, root_directory):
		self.flickr = flickr
		self.root_directory = root_directory
		self.user = flickr.test_login()
		self.user_id = self.user.find('user').attrib['id']

	def add_to_photoset(self, photoset_title, primary_photo_id):
		photosets_res = self.flickr.photosets_getList(user_id = self.user_id)
		photosets = photosets_res.find('photosets').findall('photoset')
		current_id = None
		for photoset in photosets:
			if photoset_title == photoset.find('title').text:
				current_id = photoset.attrib['id']
				self.flickr.photosets_addPhoto(photoset_id=current_id, photo_id=primary_photo_id)
				return
		self.flickr.photosets_create(title=photoset_title, primary_photo_id=primary_photo_id)

	def already_exists(self, file_hash):
		search_res = self.flickr.photos_search(user_id=self.user_id, tags=file_hash)
		photos = search_res.find('photos').findall('photo')
		if len(photos)>0:
			return photos[0].attrib['id']
		return None


	def upload_file(self, file_name, directory):
		full_path = os.path.join(directory, file_name)
		print ">>checking " + full_path
		file_hash = hashlib.md5(open(full_path).read()).hexdigest()
		existing_id = self.already_exists(file_hash)
		if not existing_id is None:
			print "already exists as " + existing_id
			return existing_id
		directory_without_root = string.replace(directory,self.root_directory, '', 1)
		tags = re.split('[^a-zA-Z0-9]+', directory_without_root, flags=re.IGNORECASE)
		tags = [tag for tag in tags if tag != '']
		if len(tags)>0:
			photoset_title = ' '.join(tags)
		else:
			photoset_title = 'uncategorized'
		tags.append(file_hash)

		def progress_callback(progress, done):
		    if done:
		        print "Done uploading"
		    else:
		        print "At %s%%" % progress
		print "uploading..."
		upload_res = self.flickr.upload(
		   title = file_name,
		   filename = full_path,
		   tags=' '.join(tags),
		   is_public = 0,
		   is_friend = 0,
		   is_family = 1,
		   callback = progress_callback )
		print "finished upload, adding to set"
		uploaded_id = upload_res.find('photoid').text
		self.add_to_photoset(photoset_title, uploaded_id)
		print "done"
		#print photoset

	def create_file_list(self):
		regex = re.compile(self.file_pattern, re.IGNORECASE)
		files_for_upload = []
		for directory, dirs, files in os.walk(self.root_directory):
			for file_name in files:
				if regex.match(file_name):
					full_path = os.path.join(directory, file_name)
					mod_time = os.path.getmtime(full_path)
					files_for_upload.append((file_name, directory, mod_time))
		files_for_upload = sorted(files_for_upload, reverse=True, key=lambda entry: entry[2])
		return files_for_upload

	def run(self):
		files_for_upload = self.create_file_list()
		#each photo will be attempted 3 times, after that 
		#it will be registered as a filure and we move on
		#if 3 consecutive phtos fail, we will stop uploading
		#and report the last exception 
		failures = 0
		for file_name, directory, mod_dat in files_for_upload:
			print "uploading " + file_name
			attempts = 0
			success = False
			while not success:
				try:
					self.upload_file(file_name, directory)
					success = True
					failures = 0
				except SystemExit:
					raise SystemExit()
				except KeyboardInterrupt:
					raise KeyboardInterrupt()
				except:
					exc_type, exc_value, exc_traceback = sys.exc_info()
					lines = traceback.format_exception(exc_type, exc_value, exc_traceback)
					print ''.join('!! ' + line for line in lines)
					attempts +=1
					print 'failed, attempts: ' + str(attempts)
					if attempts > 2:
#						failures+=1
#						print 'registering a failure:', failures
#						if failures > 2:
#							print 'Too many failures'
#							raise
						break

f = flickrapi.FlickrAPI(api_key, api_secret)
if not f.token_valid(perms=u'write'):

    # Get a request token
    f.get_request_token(oauth_callback=u'oob')

    # Open a browser at the authentication URL. Do this however
    # you want, as long as the user visits that URL.
    authorize_url = f.auth_url(perms=u'write')
    webbrowser.open_new_tab(authorize_url)

    # Get the verifier code from the user. Do this however you
    # want, as long as the user gives the application the code.
    verifier = unicode(raw_input('Verifier code: '))

    # Trade the request token for an access token
    f.get_access_token(verifier)



sync = Sync(f, root_directory)
sync.run()
